from flask import Flask, render_template, request, jsonify
from nltk.tokenize import word_tokenize
import os
import pandas as pd
import numpy as np

app = Flask(__name__)
flag=0
wq=0
data=0
whatflag=0
@app.route("/")
def hello():
    return render_template('chat1.html') # The chat interface html page
    
@app.route("/ask", methods=['POST']) # Function ask(), triggers when the message submitted in the chat window
def ask():
	global flag,wq,data,dg,whatflag
	h=0
	k=0
	n=0
	df = pd.read_csv("schemedata.csv") # Reading the dataset
	df = pd.DataFrame(df, columns = ['name','what','eligibility','certificates','procedure']) #  Reading the dataset as coloumns
	df = df.set_index(['name'], drop=False)
	m= request.form['messageText'].strip() # Input message from text box assigned to variable 'm'
	a=["അതെ","അല്ല"]
	s=word_tokenize(m)
	print(s,"tokens")
	g=df.index # Index all cell values in the hierarchy
	v=["അര്‍ഹത","അര്‍ഹതയെകുറിച്ച്","അര്‍ഹതയെപറ്റി","മാനദണ്ഡങ്ങള്‍","മാനദണ്ഡം","മാനദണ്ഡങ്ങളെപറ്റി","മാനദണ്ഡങ്ങളെകുറിച്ച്","മാനദണ്ഡത്തെപറ്റി","മാനദണ്ഡത്തെകുറിച്ച്"]# Keywords list - Keyword possibilities are listed 
	b=["രേഖകള്‍","രേഖകളെപറ്റി","രേഖകളെകുറിച്ച്","സര്‍ട്ടിഫിക്കറ്റുകള്‍","സര്‍ട്ടിഫിക്കറ്റുകളെപറ്റി","സര്‍ട്ടിഫിക്കറ്റുകളെകുറിച്ച്"]# Keywords list - Keyword possibilities are listed 
	c=["നടപടിക്രമങ്ങള്‍","നടപടി ക്രമങ്ങള്‍","നടപടിക്രമങ്ങളെപറ്റി","നടപടിക്രമങ്ങളെകുറിച്ച്","അപേക്ഷിക്കാനുള്ള നടപടിക്രമം"]# Keywords list - Keyword possibilities are listed 
	kw=["വിദ്യാഭാസ ധന സഹായം", "വിദ്യാഭാസം", "വിദ്യാഭാസ ധനസഹായം"]
	k=list(set(s).intersection(set(v))) 
	print(k)
	h=list(set(s).intersection(set(b)))
	print(h)
	n=list(set(s).intersection(set(c)))
	print(n)
	kw1=list(set(s).intersection(set(kw)))
	if m not in g:
			ad='ചോദ്യം വ്യക്തമായി ചോദിക്കാമോ?%%' # The message is out of scope
		
	if (flag==0 or flag==1) and m in g: # The message matches with any of the indexes from the dataset
		ad=m+"പദ്ധതിയെ പറ്റി ആണൊ അറിയേണ്ടത്?%%അതെ%%അല്ല%%" # Reply back to check scheme name is correct
		data=df.loc[m] # Locating the matched value after user message comparing with the indices
		print(data)
		flag=1
		print(flag)
	if flag==1 and m=="അതെ": # User confirms the query matches with response message
		ad=data.what+"ഈ പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%" # Returns short description about the queried scheme and options to know about Eligibility, Certificates required and procedure
		flag=2
		whatflag=1
	if (flag==2 or flag==3) and whatflag==0:
		if len(k)!=0 and flag==2:
			print(data.eligibility)
			if pd.isnull(data.eligibility): # When eligibility details not available
				ad="അര്‍ഹതയെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+" ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%"
			else:
				ad=data.eligibility+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%രേഖകള്‍%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല.%%" # Eligibility details and other options as buttons
		elif len(h)!=0 and flag==2:
			if pd.isnull(data.certificates): # Certificate details not available, and other options are displayed for the user to choose from
				ad="രേഖകളെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+"ഈ പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല%%"
			else:# Certificate details are provided, and other options are displayed for the user to choose from
				ad=data.certificates+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%നടപടിക്രമങ്ങള്‍%%താല്‍പര്യമില്ല%%"
			flag=2
		elif len(n)!=0 and flag==2:
			if pd.isnull(data.procedure): # procedure details not available, and other options are displayed for the user to choose from
				ad=" നടപടിക്രമങ്ങളെ പറ്റിയുള്ള വിവരങ്ങള്‍ ലഭ്യമല്ല."+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%താല്‍പര്യമില്ല%%"
			else:#Procedure details provided as response, other options are provided for user to choose from
				ad=data.procedure+"ഈ  പദ്ധതിയെ പറ്റിയുള്ള കൂടുതല്‍ വിവരങ്ങള്‍ക്ക് താഴെ തന്നിരിക്കുന്നവയില്‍ നിന്നും തെരഞ്ഞെടുക്കുക%%അര്‍ഹത%%രേഖകള്‍%%താല്‍പര്യമില്ല%%"	
			flag=2
		elif  len(k)==0 and len(n)==0 and len(h)==0 and flag==2:# When user don't want to know more about the scheme
			ad="മറ്റെതെങ്കിലും പദ്ധതിയെ പറ്റി അറിയണമോ?%%അറിയണം%%അറിയണ്ട%%"
			flag=3
		elif flag==3 and m=="അറിയണം":# When user want to know about some other scheme
			ad="പദ്ധതിയുടെ പേര് പറയുക%%"
			flag=0
		elif flag==3 and m=="അറിയണ്ട":# When user don't want to know about some other scheme
			ad="നന്ദി. കൂടുതൽ എന്തെങ്കിലും അറിയണമെങ്കിൽ പദ്ധതിയുടെ പേര് പറയുക!%%"
			flag=0

	if flag==1 and m=="അല്ല": # If the user chooses "അല്ല" option while confirming whether the query matches with the retrieved message 
		ad="പദ്ധതിയുടെ പേര് വ്യകതമായി പറയാമോ%%"
		flag=0
	
	whatflag=0	
	return jsonify({'status':'OK','answer':ad}) # Returning the response selected to the front end as JSON variable


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
