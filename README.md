# Malayalam_chatbot_SocialSecuritySchemes

This is a domain-specific retrieval based chatbot conversing in Malayalam. The domain is financial supporting schemes provided bu Govt. of Kerala.
The product is mainly a chatbot application that converses in Malayalam. The bot provides responses to the queries about social security schemes provided by Kerala government. The system is a retrieval based one. An indexing strategy is used for retrieving the appropriate response to the user queries.
The corpus is indexed and stored in the memory for later retrieval. A set of candidate responses that match the user query is pulled from the stored data using index searching mechanism. The indexing, as well as searching is performed using Dataframes of Pandas, a Python library. A short description of the scheme name user queried is provided along with options to choose other details like Eligibility, Certificated required and Procedure. The user can choose any of these options on button click to know more about the queried scheme. And an option to stop querying is also provided.
### Requirements
* Malayalam hierarchical dataset containing details about financial support schemes  provided by Kerala Government.
* Flask Package
* NLTK Package
 
</br>
- A sample experiment dataset is given in the file named "schemedata.csv" in the root directory. 

See [LICENSE](https://gitlab.com/reshmamm/malayalam_chatbot_socialsecurityschemes/blob/master/LICENSE) file for the license details.